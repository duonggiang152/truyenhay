/* eslint-disable */
import "./LoginPage.css"
import React, { useRef } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { ENV } from "../constant";
import { useHistory } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

const LoginPage = () => {
    const usernameRef = useRef(null);
    const passwordRef = useRef(null);
    const usernameSignInRef = useRef(null);
    const passwordSignInRef = useRef(null);
    const history = useHistory();
    const handleSignUp = async () => {
        const usernameValue = usernameRef.current.value;
        const passwordValue = passwordRef.current.value;
        const postData = {
            userName: usernameValue,
            password: passwordValue,
        };
        await axios.post(`${ENV.host}/api/authentication/register`, postData)
            .then((response) => {
                toast.success("Register Success", {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
            .catch((error) => {

                // Handle any errors that occurred during the request
                toast.error(error.response.data.message, {
                    position: 'top-left',
                    autoClose: 2000,
                });
                console.log(error)

            });
    };
    const handleSignIn = async () => {
        const usernameValueSignIn = usernameSignInRef.current.value;
        const passwordValueSignIn = passwordSignInRef.current.value;
        const postData = {
            userName: usernameValueSignIn,
            password: passwordValueSignIn,
        };
        await axios.post(`${ENV.host}/api/authentication/login`, postData)
            .then(async (response) => {
                localStorage.setItem('accessToken', response.data.accessToken);
                const decoded = jwt_decode(localStorage.getItem('accessToken'));
                console.log(decoded?.role[0]?.authority)
                if (decoded?.role[0]?.authority === "ADMIN") {
                    history.push('/admin');
                    history.go(0)
                }
                else {
                    history.push('/home');
                    history.go(0)
                }


            })
            .catch((error) => {
                console.log(error)
                // Handle any errors that occurred during the request
                toast.error(error.response.data.message, {
                    position: 'top-left',
                    autoClose: 2000,
                });


            });

    };
    return (
        <div className="login-container">
            <div className="main">

                <input type="checkbox" id="chk" aria-hidden="true" />

                <div className="signup">
                    <form>
                        <label htmlFor="chk" aria-hidden="true">Sign In</label>
                        <input
                            id="login-username"
                            type="text"
                            name="txt"
                            placeholder="User name"
                            required
                            ref={usernameSignInRef}
                        />
                        <input
                            id="login-password"
                            type="password"
                            name="pswd"
                            placeholder="Password"
                            required
                            ref={passwordSignInRef}
                        />
                        <button type="button" onClick={handleSignIn}>Sign In</button>
                    </form>
                </div>

                <div className="login">
                    <form>
                        <label htmlFor="chk" aria-hidden="true">Sign Up</label>
                        <input
                            id="register-username"
                            type="text"
                            name="txt"
                            placeholder="User name"
                            required
                            ref={usernameRef} // Add ref to the username input
                        />
                        <input
                            id="register-password"
                            type="password"
                            name="pswd"
                            placeholder="Password"
                            required
                            ref={passwordRef} // Add ref to the password input
                        />
                        <button type="button" onClick={handleSignUp}>Sign Up</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default LoginPage;