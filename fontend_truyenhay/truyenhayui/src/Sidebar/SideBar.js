import { useEffect, useState } from "react";
import "./SideBar.css"
import FeatherIcon from 'feather-icons-react';
import { AdminPages } from "../constant";



const SideBar = ({ handleButton }) => {
    
    const handleClick = (e) => {
    
        handleButton(e);
    };
    handleButton.bind(this)
    return (<>

        <div className="Slidebar-container">
            <nav className="navbar">
                <ul className="navbar__menu">
                    <li  className="navbar__item">
                        <a  onClick={() => handleClick(AdminPages.ComicManager)} id={AdminPages.ComicManager}  className="navbar__link">
                            <FeatherIcon icon="book-open" />
                            <span>Quản Lý Truyện</span>
                        </a>
                    </li>
                  
                    <li  className="navbar__item">
                        <a onClick={() => handleClick(AdminPages.UserManager)} id={AdminPages.UserManager}  className="navbar__link">
                            <FeatherIcon icon="users" />
                            <span>Quản Lý User</span>
                        </a>
                    </li>
                    
                </ul>
            </nav>
        </div>
    </>
    );
};

export default SideBar;