import { useEffect, useState } from "react";
import Sidebar from "../Sidebar/SideBar";
import "./admin.css"
import ComicManager from "../Component/ComicManager/ComicManger";
import UserManager from "../Component/UserManager/UserManager";
import { AdminPages } from "../constant";
import FeatherIcon from "feather-icons-react/build/FeatherIcon";
import jwt_decode from 'jwt-decode';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

const AdminPage = () => {
    const [content, setContent] = useState("comicmanager")
    const history = useHistory();
    const handdleNavBarClick = (buttomName) => {
        console.log(`new ${buttomName}  old ${content}`)
        if(buttomName !== content && buttomName) {
        setContent(buttomName)
        }
    }
    useEffect(() => {
        if(!localStorage.getItem('accessToken')) {
            history.push('/login');
            history.go(0)
            return
        }
        const decoded = jwt_decode(localStorage.getItem('accessToken'));
        if(!decoded) {
            history.push('/login');
            history.go(0)
        }

        if(decoded?.role[0]?.authority !== "ADMIN")  {
            history.push('/login');
            history.go(0)
        }

    }, [])
    handdleNavBarClick.bind(this)
    const detectComponent = () => {
        
        switch (content) {
            case AdminPages.ComicManager:
                return <ComicManager></ComicManager>
                break;
            case AdminPages.UserManager:
                return <UserManager></UserManager>
                break
            default:
                return <ComicManager></ComicManager>
                break;
        }
    }
    const handdleSignOut =async () => {
        localStorage.removeItem("accessToken");
        history.push('/login');
        history.go()
    }
    const handdleRedirectHome = async () => {
        history.push('/home');
        history.go()
    }
    return (<>

        <div className="admin-container">
            <Sidebar handleButton={handdleNavBarClick}></Sidebar>
            <div id="content">
                <div id="logo-area">
                    <img src="./favicon.ico" height={45}></img>
                    <div>Truyện Hay</div>
                </div>
                <div id="user-info">
                <div>Admin Page</div>
                <FeatherIcon onClick = {handdleRedirectHome} icon="home"/>
                <button className="sign-out" onClick={handdleSignOut}> Sign Out</button>
                </div>
              
                {detectComponent()}
               
            </div>
        </div>
    </>
    );
};

export default AdminPage;