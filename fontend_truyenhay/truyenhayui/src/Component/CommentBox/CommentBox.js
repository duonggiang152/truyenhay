import { useEffect, useState } from "react"
import { ENV } from "../../constant"
import "./CommentBox.css"
import { useHistory } from "react-router-dom/cjs/react-router-dom"
import { toast } from 'react-toastify';

const CommentBox = ({ id }) => {
    const [commentList, setCommentList] = useState([])
    const history = useHistory();
    const [commentText, setCommentText] = useState('');
    const reloadPage = () => {
        fetch(`${ENV.host}/api/comment?chapter=${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }

        })
            .then(async res => {
                const body = await res.json()
                setCommentList(body)
                console.log(body)
            })
    }
    useEffect(() => {
        reloadPage()
    }, [])
    const handdleReidrectLogin = () => {
        history.push('/login');
        history.go(0)
    }
    const handleTextChange = (e) => {
        setCommentText(e.target.value);
    };

    const handleSaveComment =async () => {
        // Call the onSaveComment callback function to save the comment
        
        console.log(commentText)
        const bearerToken = localStorage.getItem('accessToken');
        const body = {
            "chapterId": id,
            "comment":commentText
        }
        const response = await fetch(`${ENV.host}/api/comment`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${bearerToken}`,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        })
        .catch(err => {
            toast.error(err, {
                position: 'top-left',
                autoClose: 2000,
            });
        })
       
        reloadPage()
        // Clear the textarea after saving the comment
        setCommentText('');
    };
    const bearerToken = localStorage.getItem('accessToken');
    if (bearerToken) {
        return <>
            <div className="comment-detail-box">
                {commentList.map((comment) => {
                    if (comment.publicUserInfo.status === 'ENABLE') {
                        return <>
                            <div className="comment-detail">
                                <h3>User: {comment.publicUserInfo.userName}</h3>
                                <div>{comment.comment.comment}</div>
                            </div>
                        </>
                    }

                    if (comment.publicUserInfo.status === 'DISABLE') {
                        return <div className="comment-detail"> Người dùng viết comment này đã bị vô hiệu hóa do có ngôn ngữ không phù hợp </div>
                    }
                })}
                <div className="comment-send-box">
                    <textarea
                        rows="4"
                        cols="50"
                        placeholder="Enter your comment..."
                        value={commentText}
                        onChange={handleTextChange}
                    />
                    <button id="comment-send-btn" onClick={handleSaveComment}>Send</button>
                </div>
            </div>
        </>
    }
    return <>
        <div className="require-login-to-comment" onClick={() => {
            handdleReidrectLogin()
        }}>Bạn Phải Đăng Nhập Để comment</div>
    </>
}


export default CommentBox