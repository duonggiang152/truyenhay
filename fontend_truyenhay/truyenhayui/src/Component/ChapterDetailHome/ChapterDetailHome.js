import { useEffect, useState } from "react"
import { ENV } from "../../constant"
import "./ChapterDetailHome.css"
import CommentBox from "../CommentBox/CommentBox"
const ChapterDetailHome = ({ id }) => {
    const [imageList, setImageList] = useState([])
    const reloadPage = () => {
        fetch(`${ENV.host}/api/page?chapter=${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }

        })
            .then(async res => {
                const body = await res.json()
                setImageList(body)
                console.log(body)
            })
    }
    useEffect(() => {
        reloadPage()
    }, [])
    return <>
        <div className="chapter-detail-list">
            {imageList.map((image =>
                <div>
                    <img alt={image.title} title={image.title} src={`${ENV.host}/images/${image.path ? image.path : "unknowimage.jpg"}`}></img>
                </div>
            ))}
        </div>
        <div>
            <h2>Comment: </h2>
            <CommentBox id={id} />
        </div>
    </>
}

export default ChapterDetailHome;