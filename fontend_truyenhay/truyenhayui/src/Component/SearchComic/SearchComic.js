import { useState, useEffect } from "react";
import { ENV } from "../../constant";
import "./SearchComic.css"

const SearchComic = ({ setComicList }) => {

    const [tagList, setTagList] = useState([])
    const [inputSearch, setInputSearch] = useState(null)
    useEffect(() => {
        fetch(`${ENV.host}/api/tag`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async res => {
                const data = await res.json()
                console.log(data)
                setTagList(data)
                // reloadPage()
            })
            .catch(err => {
                console.log(err)
            })
    }, [])
    const findByTagCommand = (name) => {
        const params = new URLSearchParams({
            tag: name,

        });
        fetch(`${ENV.host}/api/comic?${params.toString()}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async res => {
                const data = await res.json()
                console.log(data)
                setComicList(data)
            })
            .catch(err => {
                console.log(err)
            })
    }
    const findAllComicCommand = () => {
        fetch(`${ENV.host}/api/comic`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async res => {
                const data = await res.json()
                // console.log(data)
                setComicList(data)
            })
            .catch(err => {
                console.log(err)
            })
    }
    const findWithDiscriptionCommand = () => {
        const params = new URLSearchParams({
            "description": inputSearch,
            "name": inputSearch

        });
        fetch(`${ENV.host}/api/comic?${params.toString()}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async res => {
                const data = await res.json()
                console.log(data)
                setComicList(data)
            })
            .catch(err => {
                console.log(err)
            })
    }
    return <div className="search-area">
        <div className="tagList">
            <span onClick={() => {
                findAllComicCommand()
            }}>All</span>
            {tagList.map(tag => <span onClick={() => {
                findByTagCommand(tag.name)
            }}>{tag.name}</span>)}
        </div>
        <div>
            <input placeholder="Tìm kiếm" type="text" onChange={(e) => {
                e.preventDefault()
                setInputSearch(e.target.value)
            
            }}></input>
            <button onClick={(e) => {
                e.preventDefault()
                findWithDiscriptionCommand()
            }}>Tìm</button>
        </div>
    </div>
}

export default SearchComic