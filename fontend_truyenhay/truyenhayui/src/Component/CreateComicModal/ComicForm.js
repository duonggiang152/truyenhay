import React, { useState } from "react";
import "./ComicForm.css"

const ComicForm = ({ onSubmit }) => {
    const [name, setName] = useState("");
    const [tags, setTags] = useState([]);
    const [image, setImage] = useState(null);
    const [description, setDescription] = useState("");

    const handleImageChange = (event) => {
        console.log(event.target.files[0])
        setImage(event.target.files[0]);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
     
        onSubmit({
            name,
            tags,
            image,
            description,
        });
    
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                name="name"
                placeholder="Tên"
                value={name}
                onChange={(event) => setName(event.target.value)}
            />
            <input
                type="file"
                name="image"
                onChange={handleImageChange}
            />
            <div className="preview-box" >
                {image && (
                    <img id="preview-iamge" width={300} src={URL.createObjectURL(image)} alt={image.name} />
                )}
            </div>
            <textarea
                name="description"
                placeholder="Mô tả"
                rows="auto"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
            />
            <input
                type="text"
                name="tags"
                placeholder="Tags"
                value={tags.join(", ")}
                onChange={(event) => setTags(event.target.value.split(", "))}
            />

            <button type="submit">Thêm</button>
        </form>
    );
};
export default ComicForm;