import { useState, useEffect } from "react";
import { ENV } from "../../constant";
import "./EditComic.css"
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import ChapterBrief from "../ChapterBriefAdmin/ChapterbriefAdmin";

const EditComic = ({ id,openChapterEditor }) => {
    const [comic, setComic] = useState(null)

    const [comicAvatarUrl, setComicAvatarUrl] = useState(null);
    const [comicDescription, setComicDescription] = useState(null);
    const [comicLatestChapter, setComicLatestChapter] = useState(null);
    const [comicName, setComicName] = useState(null);
    const [comicTags, setComicTags] = useState([]);
    const [image, setImage] = useState(null);
    const history = useHistory();
    const [loadingStatus, setLoadingStatus] = useState("Save")
    // id, name, avatarUrl, latestChapter, description, tags

    useEffect(() => {
        fetch(`${ENV.host}/api/comic/${id}`, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(data => {
                const { avatarUrl, description, latestChapter, name, tags } = { ...data }

                setComic(data)
                setComicAvatarUrl(avatarUrl)
                setComicDescription(description)
                setComicLatestChapter(latestChapter)
                setComicName(name)

                let arrayTag = []
                tags.forEach(arr => {
                    arrayTag.push(arr.name)
                })
                setComicTags(arrayTag)

            });
    }, [])
    const handleFileChange = (e) => {
        setImage(e.target.files[0]);
    };
    const handleContentChange = (e) => {
        if (e.target.name === "comicAvatarUrl") {
            setComicAvatarUrl(e.target.value);
        } else if (e.target.name === "comicDescription") {
            setComicDescription(e.target.value);
        } else if (e.target.name === "comicLatestChapter") {
            setComicLatestChapter(e.target.value);
        } else if (e.target.name === "comicName") {
            setComicName(e.target.value);
        } else if (e.target.name === "comicTags") {
            const listTag = e.target.value.split(",")
            setComicTags(listTag);
        }
    };
    const handleFormSubmit = (e) => {
        e.preventDefault();

        // Update the comic content in your backend

        // Close the form

    };
    if (!comic === null) return <div>Loading data</div>

    const submitFunction = async () => {
        const bearerToken = localStorage.getItem('accessToken');
        const formData = new FormData();
        formData.append("comicId", id);
        formData.append("banner", image);
        setLoadingStatus("Saving")
        if (image) {
            const response = await fetch(`${ENV.host}/api/comic/banner`, {
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${bearerToken}`,
                },
                body: formData,
            })
                .then(async res => {
                    const body = await res.json()
                    const formData = new FormData();
                    formData.append("description", comicDescription);
                    formData.append("tags", comicTags);
                    formData.append("name", comicName);
                    await fetch(`${ENV.host}/api/comic/update/${id}`, {
                        method: "POST",
                        headers: {
                            "Authorization": `Bearer ${bearerToken}`,
                            
                        },
                        body: formData,
                    })
                        .catch(err => {
                            console.log(err)
                        })


                })
                .then(() => {
                    setLoadingStatus("Save")
                })
                .catch(err => {
                    console.log(err)
                })
        }
        else {
            const formData = new FormData();
            formData.append("description", comicDescription);
            formData.append("tags", comicTags);
            formData.append("name", comicName);
            await fetch(`${ENV.host}/api/comic/update/${id}`, {
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${bearerToken}`,
                },
                body: formData,
            }).then(async data => {
                const body = await data.json()
            }).then(() => {
                setLoadingStatus("Save")
            })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    const handdleTags = (e) => {

        const tags = e.target.value.split(",")

        setComicTags(tags)
    }

    let inputTags = ""
    comicTags.forEach((tag, i) => {
        if (i === 0) inputTags = tag
        else inputTags += "," + tag
    })
    return (
        <>
            <div className="edit-comic">
                <div className="form-update-comic">
                    <form onSubmit={handleFormSubmit}>
                        <h1>
                            <input
                                type="file"
                                name="comicAvatarFile"
                                onChange={handleFileChange}
                            />
                            <div className="comic-avatar-view">
                                {
                                    (() => {
                                        if (image) return <img id="preview-iamge" src={URL.createObjectURL(image)} alt={image.name} />
                                        return <img width={300} src={`${ENV.host}/images/${comicAvatarUrl ? comicAvatarUrl : "unknowimage.jpg"}`} alt="Comic avatar" />
                                    })()
                                }

                            </div>
                        </h1>
                        <p>
                            <input
                                type="text"
                                name="comicName"
                                value={comicName}
                                onChange={handleContentChange}
                            />
                        </p>
                        <p>
                            <textarea
                                name="comicDescription"
                                value={comicDescription}
                                onChange={handleContentChange}
                            />
                        </p>
                        <p>
                            <input
                                type="text"
                                name="comicTags"
                                value={inputTags}
                                onChange={handdleTags}
                            />
                        </p>
                        <button type="submit" onClick={submitFunction}>{loadingStatus}</button>
                    </form>
                </div>
                <ChapterBrief openChapterEditor = {openChapterEditor} id={id} />
            </div>
        </>
    );
}

export default EditComic