import { useEffect, useState } from "react";
import "./ChapterDetail.css"
import Modal from 'react-modal';
import FeatherIcon from "feather-icons-react/build/FeatherIcon";
import { ENV } from "../../constant";
import { toast } from 'react-toastify';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: "500px",
    },
};
const ChapterDetail = ({ id }) => {
    const [modalIsOpen, setIsOpen] = useState(false);
    const [modalEditIsOpen, setEditIsOpen] = useState(false);
    const [image, setImage] = useState(null);
    const [editImage, setEditImage] = useState(null);
    const [name, setName] = useState("")
    const [imageList, setImageList] = useState([])
    const [updatingId, setUpdatingId] = useState("")
    const reloadPage = () => {
        fetch(`${ENV.host}/api/page?chapter=${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }

        })
            .then(async res => {
                const body = await res.json()
                setImageList(body)
                console.log(body)
            })
    }
    useEffect(() => {
        reloadPage()
    }, [])
    function closeModal() {
        setIsOpen(false);
    }
    function openModal() {
        setIsOpen(true)
    }
    function closeEditModal() {
        setEditIsOpen(false);
    }
    function openEditModal(id) {
        setUpdatingId(id)
        setEditIsOpen(true)
    }
    const handleImageChange = (event) => {
        console.log(event.target.files[0])
        setImage(event.target.files[0]);
    };
    const handleEditImageChange = (event) => {
        console.log(event.target.files[0])
        setEditImage(event.target.files[0]);
    };
    const handdleSubmit =async (e) => {
        e.preventDefault()
        const body = {
            "tittle": name,
            "chapterId": id
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/page`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
            .then(async res => {
                if(!image) return
                const body = await res.json()
                console.log(body)
                const formdata = new FormData()
                formdata.append("id", body.id)
                formdata.append("page", image)
                await fetch(`${ENV.host}/api/page/update-image`, {
                    method: 'POST',
                    headers: {
                        "Authorization": `Bearer ${bearerToken}`,
                    },
                    body: formdata
                })
                    .then(async res => {
                        const body = await res.json()


                    })
            })
        closeModal()
        reloadPage()
    }
    const handdleUpdate =async (e) => {
        e.preventDefault()
        const formdata = new FormData()
        formdata.append("id", updatingId)
        formdata.append("page", editImage)
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/page/update-image`, {
            method: 'POST',
            headers: {
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: formdata

        })
        .catch(err => {
            console.log(err)
        })
        reloadPage()
        closeModal()
    }
    const handdleDelete =async (id) => {
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/page/${id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
        })
            .catch(err => {
                toast.error(err, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
        reloadPage()
    }
    const executeUpOrder = async (pageId) => {
        const body = {
            pageId
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/chapter/up-page`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
        .catch(err => {
            toast.error(err, {
                position: 'top-left',
                autoClose: 2000,
            });
        })
        reloadPage()
    }
    const executeDownOrder = async (pageId) => {
        const body = {
            pageId
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/chapter/down-page`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
        .catch(err => {
            toast.error(err, {
                position: 'top-left',
                autoClose: 2000,
            });
        })
        reloadPage()
    }
    return <div className="detail-chapter-edit">
        <Modal
            isOpen={modalIsOpen}
            // onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <div className="close-modal">
                <button onClick={closeModal}>X</button>
            </div>
            <div>Thêm tranh mới</div>
            <form onSubmit={handdleSubmit}>
                <input
                    type="text"
                    name="name"
                    placeholder="Tên"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                />
                <input
                    type="file"
                    name="image"
                    onChange={handleImageChange}
                />
                <div className="preview-box" >
                    {image && (
                        <img id="preview-iamge" width={300} src={URL.createObjectURL(image)} alt={image.name} />
                    )}
                </div>
                <button type="submit">Thêm</button>
            </form>
        </Modal>

        <Modal
            isOpen={modalEditIsOpen}
            onRequestClose={closeEditModal}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <div className="close-modal">
                <button onClick={closeEditModal}>X</button>
            </div>
            <div>Sửa tranh</div>
            <form onSubmit={handdleUpdate}>

                <input
                    type="file"
                    name="image"
                    onChange={handleEditImageChange}
                />
                <div className="preview-box" >
                    {editImage && (
                        <img id="preview-iamge" width={300} src={URL.createObjectURL(editImage)} alt={editImage.name} />
                    )}
                </div>
                <button type="submit">Thêm</button>
            </form>
        </Modal>

        <div>
            <div className="list-page-detail">
                {imageList.map(image => {
                    return <div className="page-detail">
                        <div>
                            <div>{image.tittle}:</div>
                            <FeatherIcon onClick={() => {
                                handdleDelete(image.id)
                            }} icon="trash" size={15} />
                            <FeatherIcon onClick={() => {
                                openEditModal(image.id)
                            }} icon="edit" size={15} /> 

                        <FeatherIcon onClick={() => {
                          executeUpOrder(image.id)
                        }} icon="arrow-up" size={15} />
                        <FeatherIcon onClick={() => {
                            executeDownOrder(image.id)
                        }} icon="arrow-down" size={15} />

                        </div>
                        <img width={"200em"} height={"200em"} src={`${ENV.host}/images/${image.path ? image.path : "unknowimage.jpg"}`}></img>

                    </div>
                })}
            </div>
            <button onClick={openModal}>
                <FeatherIcon icon="plus" />
            </button>
        </div>
    </div>
}
export default ChapterDetail