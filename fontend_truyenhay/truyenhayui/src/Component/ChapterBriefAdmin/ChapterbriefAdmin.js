import { useEffect, useState } from "react"
import { ENV } from "../../constant"
import FeatherIcon from "feather-icons-react/build/FeatherIcon"
import "./ChapterBriefAdmin.css"
import { toast } from 'react-toastify';
import Modal from 'react-modal';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: "500px",

    },
};
const ChapterBrief = ({ id, openChapterEditor }) => {
    const [newName, setNewName] = useState("")
    const [currentChapter, setCurrentChapter] = useState([])
    const [modalIsOpen, setIsOpen] = useState(false);
    const [placeHolderUpdareNameChapter, setPlaceHolderUpdateNameChapter] = useState("")
    const [updateChapterTittle, setUpdateChapterTittle] = useState("")
    const [updateingId, setupdatingId] = useState(0)
    function closeModal() {
        setIsOpen(false);
    }
    function openModal(placeHolder, id) {
        setPlaceHolderUpdateNameChapter(placeHolder)
        setupdatingId(id)
        setIsOpen(true)
    }
    const reloadChapter = () => {
        fetch(`${ENV.host}/api/chapter/${id}`, {
            method: 'GET',
        })
            .then(
                async res => {
                    const data = await res.json()
                    if (res.status === 200)
                        setCurrentChapter(data)

                }
            )
    }
    useEffect(() => {
        reloadChapter()
    }, id)
    const handdleNewNameChange = (e) => {
        const newname = e.target.value;
        setNewName(newname)
    }
    const handdleNewNameTittleUpdateChange = (e) => {
        const newname = e.target.value;
        setUpdateChapterTittle(newname)
    }
    const handdleNewChapter = async (e) => {
        const body = {
            comicId: id,
            name: newName
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/chapter`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
            .catch(err => {
                toast.error(err, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
        reloadChapter()
    }
    const handdlerNewupdateChapter = async (e) => {
        const body = {
            id: updateingId,
            name: updateChapterTittle
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/chapter`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
            .then(async data => {
                const d = await data.json()
                console.log(d)
            })
            .catch(err => {
                toast.error(err, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
        reloadChapter()
        closeModal()
    }
    const deleteChapter = async (id) => {
        await fetch(`${ENV.host}/api/chapter/${id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",

            },
        })
            .catch(err => {
                toast.error(err, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
        reloadChapter()
    }
    const executeUpOrderChapterCommand = async (chapterId) => {
        const body = {
            chapterId
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/comic/up-chapter`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
        .catch(err => {
            toast.error(err, {
                position: 'top-left',
                autoClose: 2000,
            });
        })
        reloadChapter()
    }
    const executeDownOrderChapterCommand = async (chapterId) => {
        const body = {
            chapterId
        }
        const bearerToken = localStorage.getItem('accessToken');
        await fetch(`${ENV.host}/api/comic/down-chapter`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(body)

        })
        .catch(err => {
            toast.error(err, {
                position: 'top-left',
                autoClose: 2000,
            });
        })
        reloadChapter()
    }
    return <>
        <Modal
            isOpen={modalIsOpen}
            // onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <div className="close-modal">
                <button onClick={closeModal}>X</button>
            </div>
            <div>Chỉnh sửa tên chương truyện</div>
            <div className="add-chapter-contaienr">
                <div className="add-btn">
                    <input placeholder={placeHolderUpdareNameChapter} onChange={handdleNewNameTittleUpdateChange} ></input>
                    <button onClick={handdlerNewupdateChapter}>Save</button>

                </div>
            </div>
        </Modal>
        <div className="chapter-list">
            <div>
                Chapter:
            </div>
            <div className="chapter-container">

                {currentChapter.map((chapter, i) =>
                    <div className="chapter-container-child" >
                        <div>{i+1}</div>
                        <div onClick={() => { openChapterEditor(chapter.chapterName, chapter.id) }}>{chapter.chapterName}</div>
                        <FeatherIcon onClick={() => {
                            deleteChapter(chapter.id)
                        }} icon="trash" size={15} />
                        <FeatherIcon onClick={() => {
                            openModal(chapter.chapterName, chapter.id)
                        }} icon="edit" size={15} />
                        <FeatherIcon onClick={() => {
                            executeUpOrderChapterCommand(chapter.id)
                        }} icon="arrow-up" size={15} />
                        <FeatherIcon onClick={() => {
                            executeDownOrderChapterCommand(chapter.id)
                        }} icon="arrow-down" size={15} />
                    </div>)
                }

            </div>
            <div className="add-chapter-contaienr">
                <div className="add-btn">
                    <FeatherIcon icon="plus" size={80} />

                    <input onChange={handdleNewNameChange} ></input>
                    <button onClick={handdleNewChapter}>Save</button>

                </div>
            </div>
        </div>
    </>

}

export default ChapterBrief