/* eslint-disable */

import { ENV } from "../../constant";
import "./ComicBriefHome.css"

const ComicBriefHome = ({ id, name, avatarUrl, tags, description, openComicDetail }) => {
    const onClick = () => {
        openComicDetail(name, id);
    };

    return (<>
        <div className="comic-brief-container">

            <div className="comic-brief" onClick={onClick}>
            
                <img width={"200em"} height={"200em"} src={`${ENV.host}/images/${avatarUrl ? avatarUrl : "unknowimage.jpg"}`}></img>

                <div className="brief">
                    <div className="header-brief">
                        {name}
                    </div>

                    <div>
                        <span>Mô tả: </span>{description ? description : ""}
                    </div>
                    <div>
                        <span>Loại: </span>{tags.map(tag => tag.name + ",")}
                    </div>
                </div>
            </div>
        </div>
    </>
    );
};

export default ComicBriefHome;