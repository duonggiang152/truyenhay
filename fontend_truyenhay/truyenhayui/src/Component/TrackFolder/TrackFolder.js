import { AdminEditRender } from '../../constant'
import './TrackFolder.css'


const TrackFolder = ({ trackList, setRenderState }) => {



    function handdle(type, id) {
        setRenderState({ type: type, id: id })
    }
    return <>
        <div className="track-list">
            <div onClick={() => { handdle(AdminEditRender.COMICLIST) }}>Danh sách truyện  </div>

            {trackList.map((track, i) =>
                <div onClick={() => {
                    if (i == 0) {
                        handdle(AdminEditRender.COMICDETAILS, track.id)
                    }
                    else if (i == 1) {
                        handdle(AdminEditRender.CHAPTERDETAILS, track.id)
                    }
                }}> &gt;
                    {(() => {
                        if (i == 0) {
                            return <span>Truyện: </span>
                        }
                        else if (i == 1) {

                            return <span>Chương: </span>

                        }
                    })()} {track.name}</div>)
            }
        </div>
    </>
}
export default TrackFolder