/* eslint-disable */

import { useEffect, useState } from "react";
import { AdminEditRender, ENV } from "../../constant";
import ComicBrief from "../ComicBriefAdmin/ComicBriefAdmin";
import "./ComicManager.css"
import FeatherIcon from "feather-icons-react/build/FeatherIcon";
import Modal from 'react-modal';
import ComicForm from "../CreateComicModal/ComicForm";
import TrackFolder from "../TrackFolder/TrackFolder";
import { useHistory } from 'react-router-dom';
import EditComic from "../EditComic/EditComic";
import ChapterDetail from "../ChapterDetailAdmin/ChapterDetailAdmin";
import { toast } from 'react-toastify';
import SearchComic from "../SearchComic/SearchComic";


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: "500px",
    },
};


const ComicManager = () => {
    const history = useHistory();
    const [listComic, setListComic] = useState([])
    const [trackList, setTrackList] = useState([])
    const [render, setRender] = useState({ type: AdminEditRender.COMICLIST }) // {type, id}
    const fetchListComic  = () => {
        fetch(`${ENV.host}/api/comic`, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(data => {
                setListComic(data)
            })
            .catch(error => {
                toast.error(error, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
    }
    useEffect(() => {
        fetchListComic()
    }, [])
    const reloadPage = () => {
        fetchListComic()
    }
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }


    async function handdleSubmitModal({
        name,
        tags,
        image,
        description,
    }) {

        const createComicRequest = {
            name,
            tagsName: tags,
            description,
        };
        const bearerToken = localStorage.getItem('accessToken');
        const response = await fetch(`${ENV.host}/api/comic`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${bearerToken}`,
            },
            body: JSON.stringify(createComicRequest),
        })
            .then(async res => {
                const data = await res.json()
                console.log(data)
                if (!image) return "ok"
                console.log(data.newComic.id)
                return data.newComic.id

            })
            .then(async id => {

                if (!image) return
                const formData = new FormData();
                formData.append("comicId", id);
                formData.append("banner", image);
                await fetch(`${ENV.host}/api/comic/banner`, {
                    headers: {
                        "Authorization": `Bearer ${bearerToken}`,
                    },
                    method: "POST",
                    body: formData,
                })
                    .then(async res => {
                        const body = await res.json()
                        const data = body.newComic
                        setListComic([data, ...listComic])
                    })
            })

            .catch(err => {
                history.push('/login');
                history.go(0)
            })
        closeModal()
    }

    function closeModal() {
        setIsOpen(false);
    }

    function openComicEditor(name, id) {
        setTrackList([{ name: name, id: id }])
        setRender({ type: AdminEditRender.COMICDETAILS, id: id })
    }

    function openChapterEditor(chapterName, chapterId) {

        if (trackList.length <= 1)
            setTrackList([...trackList, { name: chapterName, id: chapterId }])
        if (trackList.length == 2) {
            setTrackList([trackList[0], { name: chapterName, id: chapterId }])
        }
        setRender({ type: AdminEditRender.CHAPTERDETAILS, id: chapterId })
    }

    function renderContentEdit() {
        if (render.type === AdminEditRender.COMICLIST) {
            return<>
               <SearchComic setComicList = {setComicListCommand} />
             <div className="comic-list"> {
                listComic.map((item) => {
                    return <ComicBrief {...item} openComicEditor={openComicEditor} reloadPage = {reloadPage} ></ComicBrief>
                })
            }</div>
            </>
        }
        if (render.type === AdminEditRender.COMICDETAILS) {
            return <EditComic openChapterEditor={openChapterEditor} id={render.id} />
        }
        if (render.type === AdminEditRender.CHAPTERDETAILS) {
            return <ChapterDetail id={render.id} />
        }
        return <></>
    }

    function setRenderState({ type, id }) {
        setRender({ type, id })
        if (type === AdminEditRender.COMICLIST) {
           fetchListComic()
           setTrackList([])
        }
        if (type === AdminEditRender.COMICDETAILS)
            setTrackList([trackList[0]])
    }
    function setComicListCommand(listComic)  {
        if(typeof listComic === typeof []) setListComic(listComic)
    }
    return (<>
        <div>

            <Modal
                isOpen={modalIsOpen}
                // onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <div className="close-modal">
                    <button onClick={closeModal}>X</button>
                </div>
                <div>Thêm truyện tranh mới</div>
                <ComicForm onSubmit={handdleSubmitModal} />
            </Modal>
        </div>
        <div className="comicmanager">

            <h3>Quản Lý truyện tranh</h3>


            <div id="comic-list-manager">
                <div id="comicmanager-header">
                    <button onClick={openModal}>
                        <FeatherIcon icon="plus" />
                    </button>

                    <TrackFolder trackList={trackList} setRenderState={setRenderState} />
                </div>
             
                {renderContentEdit()}

            </div>
        </div>
    </>
    );
};

export default ComicManager;