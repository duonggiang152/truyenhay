/* eslint-disable */

import { useEffect, useState } from "react";
import { ENV } from "../../constant";
import "./UserManager.css"

const UserManager = () => {
    const [userPublicInfo, setUserPublicInfo] = useState([])
    const fetchListUser = () => {
        fetch(`${ENV.host}/api/user`, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setUserPublicInfo(data)
            })
            .catch(error => {
                toast.error(error, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
    }
    useEffect(() => {
        fetchListUser()
    }, [])
    const toggleUser = async (id) => {
        let currentStatus = null;
        for (let i = 0; i < userPublicInfo.length; i++) {
            if (userPublicInfo[i].id === id)
                currentStatus = userPublicInfo[i].status
        }

        if (!currentStatus) return;
        const body = {
            "id": id,
            "userStatus": (currentStatus == "ENABLE" ? "DISABLE" : "ENABLE")
        }
        fetch(`${ENV.host}/api/user/status`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)

        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                fetchListUser()
                
            })
            .catch(error => {
                toast.error(error, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
    };
    return (<>

        <div className="usermanager">

            <h3>Quản Lý Người dùng</h3>
            <div className="list-user-manager">
                {userPublicInfo.map((user) => {
                    return <div className="container-brief">
                        <div>Người dùng: {user.userName} </div>
                        <div className={`user-button ${user.status === "ENABLE" ? 'active' : 'inactive'}`}>
                            <button onClick={() => { toggleUser(user.id) }}>
                                {user.status === "ENABLE" ? 'ENABLE' : 'DISABLE'}
                            </button>
                        </div>
                    </div>
                })}
            </div>
        </div>
    </>
    );
};

export default UserManager;