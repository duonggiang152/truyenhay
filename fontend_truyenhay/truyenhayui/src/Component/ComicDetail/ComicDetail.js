import { useEffect, useState } from "react";
import { ENV } from "../../constant";
import "./ComicDetail.css"

const ComicDetail = ({ id ,openChapterDetail}) => {
    const [data, setData] = useState({})
    const [chapterList, setChapterList] = useState([])
    const loadData = () => {
        fetch(`${ENV.host}/api/comic/${id}`, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setData(data)
            });
    }
    const loadChapter = () => {
        fetch(`${ENV.host}/api/chapter/${id}`, {
            method: 'GET',
        })
            .then(async res => {
                    const data = await res.json()
                    console.log(data)
                    setChapterList(data)

                }
            )
    }
    useEffect(() => {
        loadData()
        loadChapter()
    },[])

    const handdleOpenChapter = (name, id) => {
        openChapterDetail(name, id)
    }
    let { avatarUrl, description, latestChapter, name, tags} = data;
    if(!tags) tags = []
    return <>
        <div className="comicdetail">
            <div className="comic-info">
                <div>
                    <img width={"200em"} height={"200em"} src={`${ENV.host}/images/${avatarUrl ? avatarUrl : "unknowimage.jpg"}`}></img>

                </div>
                <div className="comic-brief-info">
                    <h3 className="name">{name}</h3>
                    <div className="description">Mô tả: {description}</div>
                    <div className="tags">
                        <div>Thể loại: </div>
                        {tags.map(tag => <div className="comic-tag">{tag.name} </div>)}
                    </div>
                </div>
            </div>
            <div className="list-chapter">
                <div>
                    Danh sách chương: 
                </div>
                {chapterList.map((chapter,i) =>
                 <div className="chapter-brief">
          
                    <div onClick={() => {handdleOpenChapter(chapter.chapterName, chapter.id)}}>{chapter.chapterName}</div>
                </div>
                )}
            </div>
        </div>
    </>
}

export default ComicDetail;