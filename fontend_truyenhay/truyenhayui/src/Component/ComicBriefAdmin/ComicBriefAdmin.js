/* eslint-disable */

import { ENV } from "../../constant";
import "./ComicBriefAdmin.css"

const ComicBrief = ({ reloadPage, id, name, avatarUrl, tags, latestChapter, description, openComicEditor }) => {
    const onClick = () => {
        openComicEditor(name, id);
    };

    const deleteF = () => {
        fetch(`${ENV.host}/api/comic/${id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(async res => {
            reloadPage()
        })
    }

    return (<>
        <div className="comic-brief-container">

            <div className="comic-brief" onClick={onClick}>
            
                <img width={"200em"} height={"200em"} src={`${ENV.host}/images/${avatarUrl ? avatarUrl : "unknowimage.jpg"}`}></img>

                <div className="brief">
                    <div className="header-brief">
                        {name}
                    </div>

                    <div>
                        <span>Mô tả: </span>{description ? description : ""}

                    </div>
                    <div>
                        <span>Loại: </span>{tags.map(tag => tag.name + ",")}
                    </div>
                </div>
            </div>
            <div className="delete-brief" onClick={deleteF}>xóa</div>
        </div>
    </>
    );
};

export default ComicBrief;