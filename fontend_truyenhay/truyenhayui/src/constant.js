const ENV= {
    "host": "http://localhost:1111"
}

const AdminPages = {
    ComicManager: "comicmanager",
    UserManager: "usermanager"

}
const AdminEditRender = {
    COMICLIST: "comic-list",
    COMICDETAILS: "comic-details",
    CHAPTERDETAILS: "chapter-details"
}

module.exports.AdminEditRender = AdminEditRender
module.exports.ENV = ENV
module.exports.AdminPages = AdminPages