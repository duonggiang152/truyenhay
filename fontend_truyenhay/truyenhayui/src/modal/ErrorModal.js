/* eslint-disable */
import React from 'react';
import Modal from 'react-modal';

const ErrorModal = ({ isOpen, onRequestClose, errorMessage }) => {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Error Modal"
        >
            <h2 >Error</h2>
            <p>{errorMessage}</p>
            <button onClick={onRequestClose}>Close</button>
        </Modal>
    );
};

export default ErrorModal;