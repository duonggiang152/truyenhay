import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LoginPage from './login/LoginPage';
import HomePage from './Home/Home';
import AdminPage from './Admin/Admin';
import Modal from 'react-modal';

Modal.setAppElement('#root');
function App() {
  return (
    <Router>
      <Switch>

        <Route path="/login" component={LoginPage} />
        <Route path="/home" component={HomePage} />
        <Route path="/admin" component={AdminPage} />
        <Route path="/*" component={HomePage} />
      </Switch>
    </Router>
  );
}

export default App;
