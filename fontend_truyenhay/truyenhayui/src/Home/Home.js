/* eslint-disable */

import FeatherIcon from "feather-icons-react/build/FeatherIcon";
import "./Home.css"
import TrackFolder from "../Component/TrackFolder/TrackFolder";
import { useEffect, useState } from "react";
import ComicBrief from "../Component/ComicBriefAdmin/ComicBriefAdmin";
import { AdminEditRender, ENV } from "../constant";
import ComicList from "../Component/ComicBriefHome/ComicBriefHome";
import ComicBriefHome from "../Component/ComicBriefHome/ComicBriefHome";
import ComicDetail from "../Component/ComicDetail/ComicDetail";
import ChapterDetailHome from "../Component/ChapterDetailHome/ChapterDetailHome";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import jwt_decode from "jwt-decode";
import SearchComic from "../Component/SearchComic/SearchComic";

const HomePage = () => {
    const [trackList, setTrackList] = useState([])
    const [listComic, setListComic] = useState([])
    const [render, setRender] = useState({ type: AdminEditRender.COMICLIST })
    const [isLogedIn, setIsLogedin] = useState(false)
    const history = useHistory();
    const fetchListComic  = () => {
        fetch(`${ENV.host}/api/comic`, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setListComic(data)
            })
            .catch(error => {
                toast.error(error, {
                    position: 'top-left',
                    autoClose: 2000,
                });
            })
    }
    useEffect(() => {
        fetchListComic()
    }, [])
    const setRenderState = ({ type: type, id: id }) => {
        setRender({ type, id })
        if (type === AdminEditRender.COMICLIST) {
           fetchListComic()
           setTrackList([])
        }
        if (type === AdminEditRender.COMICDETAILS)
            setTrackList([trackList[0]])
    }

    function openChapterDetail(chapterName, chapterId) {

        if (trackList.length <= 1)
            setTrackList([...trackList, { name: chapterName, id: chapterId }])
        if (trackList.length == 2) {
            setTrackList([trackList[0], { name: chapterName, id: chapterId }])
        }
        setRender({ type: AdminEditRender.CHAPTERDETAILS, id: chapterId })
    }

    function openComicDetail(name, id) {
        setTrackList([{ name: name, id: id }])
        setRender({ type: AdminEditRender.COMICDETAILS, id: id })
    }
    function setComicListCommand(listComic)  {
        if(typeof listComic === typeof []) setListComic(listComic)
    }
    function renderContentEdit() {
        if (render.type === AdminEditRender.COMICLIST) {
            return <>
            <SearchComic setComicList = {setComicListCommand} />
            <div className="comic-list"> {
                listComic.map((item) => {
                    return <ComicBriefHome {...item} openComicDetail = {openComicDetail}  />
                })
            }</div>
            </>
        }
        if (render.type === AdminEditRender.COMICDETAILS) {
            return <ComicDetail id={render.id} openChapterDetail = {openChapterDetail}/>
        }
        if (render.type === AdminEditRender.CHAPTERDETAILS) {
            return <ChapterDetailHome id={render.id} />
        }
        return <></>
    }
    
    
    // const bearerToken = localStorage.getItem('accessToken');
    // if(bearerToken && !setIsLogedin) {
    //    setIsLogedin(true)
    // }
    const handdleSignOut = () => {
        localStorage.removeItem("accessToken");
        history.push('/login');
        history.go()
    }
    const decodeJwt = () => {
        const bearerToken = localStorage.getItem('accessToken');
        if(!bearerToken) return
        const decoded = jwt_decode(bearerToken);
 
        return decoded
    }

    return (<>
        
        <div className="home-container">
            <div id="logo-area">
                <img src="./favicon.ico" height={45}></img>
                <div>Truyện Hay</div>
            </div>
            <div id="content-home">
                <div id="user-info">
                <div>
                    HomePage
                </div>
                <FeatherIcon icon="home"/>
                {
                    (() => {
                        if(localStorage.getItem('accessToken')) {
                                return <button className="sign-out" onClick={handdleSignOut}> Sign Out</button>
                        }
                        else return
                    })()
                }
                {
                    (() => {
                        if(localStorage.getItem('accessToken')) {
                                return <div> User: {decodeJwt().sub} </div>
                        }
                        else return
                    })()
                }
                </div>
                <TrackFolder trackList={trackList}  setRenderState = {setRenderState}/>
                {renderContentEdit()}
            </div>
        </div>
    </>
    );
};

export default HomePage