package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Helper.GrantedAuthorityAccount;
import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Model.UserDetailsImplementation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class UserDetailsServiceImplement implements UserDetailsService {


	private final UserService userService;

	private List<GrantedAuthorityAccount> getGrantedAuthority(Long id) {
		User user = userService.findById(id);
		GrantedAuthorityAccount grantedAuthority = new GrantedAuthorityAccount();
		grantedAuthority.setAuthority(user.getRole().toString());
		List<GrantedAuthorityAccount> gratedAuthories = new ArrayList();
		gratedAuthories.add(grantedAuthority);
		return gratedAuthories;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException  {
		User user =  userService.findByName(username);
		List grantedAuthorities = getGrantedAuthority(user.getId());
		return new UserDetailsImplementation(user.getId(), user.getUserName(), grantedAuthorities);
	}
}
