package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Repo.ChapterRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
@Lazy
public class OrderPageService {
	private final ChapterService chapterService;
	private final ChapterRepository chapterRepository;

	public void deleteChapterTracePages(Long chapterId, Long pageId) {
		Chapter chapter = chapterService.findById(chapterId);
		String pageOrderRaw = chapter.getPages();
		List<String> pagesInOrder = new ArrayList<>(Arrays.asList(pageOrderRaw.split(",")));
		pagesInOrder.remove(String.valueOf(pageId));
		String newTraceList = "";
		for (int i = 0; i < pagesInOrder.size(); i++) {
			if (newTraceList.isEmpty() || newTraceList.isBlank()) {
				newTraceList = pagesInOrder.get(i);
			} else {
				newTraceList = newTraceList + "," + pagesInOrder.get(i);
			}
		}

		chapter.setPages(newTraceList);
		chapterRepository.save(chapter);
	}

	public List<Page> orderChapter(Long chapterId, List<Page> inputPages) {
		Chapter chapter = chapterService.findById(chapterId);
		String orderChapterString = chapter.getPages();
		if(orderChapterString == null) orderChapterString = "";
		String[] order = orderChapterString.split(",");
		List<Long> orderIds = new ArrayList<>();
		for (int i = 0; i < order.length; i++) {
			if (order[i].isEmpty() == false && order[i].isBlank() == false)
				orderIds.add(new Long(order[i]));
		}
		List<Page> resultOrder = new ArrayList<>();
		for (int i = 0; i < orderIds.size(); i++) {
			for (int j = 0; j < inputPages.size(); j++) {
				if (orderIds.get(i).equals(inputPages.get(j).getId())) {
					resultOrder.add(inputPages.get(j));
				}
			}
		}
		return resultOrder;

	}

	private String  arrayOrderToStringOrder(String[] arrayOrder) {

		String newPageOrderRaw = "";
		for(int i = 0 ; i < arrayOrder.length; i++) {
			if(i == 0) {
				newPageOrderRaw = arrayOrder[i];
			}
			else {
				newPageOrderRaw += "," + arrayOrder[i];
			}
		}
		return newPageOrderRaw;
	}

	public String upPageOrder(Page page) {
		Long chapterId = page.getChapterId();
		Chapter chapter = chapterService.findById(chapterId);
		String pageOrderRaw = chapter.getPages();
		String[] pagesInOrder = pageOrderRaw.split(",");
		for(int i = 1 ; i < pagesInOrder.length; i++) {
			if(pagesInOrder[i].compareTo(page.getId().toString()) == 0) {
				String temp = pagesInOrder[i];
				pagesInOrder[i] = pagesInOrder[i-1];
				pagesInOrder[i-1] = temp;
			}
		}
		String newOrderRaw= arrayOrderToStringOrder(pagesInOrder);
		chapter.setPages(newOrderRaw);
		chapterRepository.save(chapter);
		return newOrderRaw;
	}

	public String downPageOrder(Page page) {
		Long chapterId = page.getChapterId();
		Chapter chapter = chapterService.findById(chapterId);
		String pageOrderRaw = chapter.getPages();
		String[] pagesInOrder = pageOrderRaw.split(",");
		for(int i = 0 ; i < pagesInOrder.length-1; i++) {
			if(pagesInOrder[i].compareTo(page.getId().toString()) == 0) {
				String temp = pagesInOrder[i];
				pagesInOrder[i] = pagesInOrder[i+1];
				pagesInOrder[i + 1] = temp;
				break;
			}
		}
		String newOrderRaw= arrayOrderToStringOrder(pagesInOrder);
		chapter.setPages(newOrderRaw);
		chapterRepository.save(chapter);
		return newOrderRaw;
	}
}
