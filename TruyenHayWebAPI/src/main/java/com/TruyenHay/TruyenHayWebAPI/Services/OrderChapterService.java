package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Repo.ComicRepository;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.ComicService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
@Lazy
public class OrderChapterService {
	private final ComicService comicService;

	private final ComicRepository comicRepository;

	public void deleteComicTraceChapter(Long comicId, Long chapterId) {
		Comic comic = comicService.findById(comicId);
		String chapterOrderRaw = comic.getChapters();
		List<String> chaptersInOrder = new ArrayList<>(Arrays.asList(chapterOrderRaw.split(",")));
		chaptersInOrder.remove(String.valueOf(chapterId));
		String newOrderChapter = arrayOrderToStringOrder(chaptersInOrder.toArray(String[]::new));

		comic.setChapters(newOrderChapter);
		comicRepository.save(comic);
	}

	private String arrayOrderToStringOrder(String[] arrayOrder) {

		String newPageOrderRaw = "";
		for (int i = 0; i < arrayOrder.length; i++) {
			if (i == 0) {
				newPageOrderRaw = arrayOrder[i];
			} else {
				newPageOrderRaw += "," + arrayOrder[i];
			}
		}
		return newPageOrderRaw;
	}

	public String upChapterOrder(Chapter chapter) {
		Long comicId = chapter.getComicId();
		Comic comic = comicService.findById(comicId);
		String chapterOrderRaw = comic.getChapters();
		String[] chaptesInOrder = chapterOrderRaw.split(",");
		for (int i = 1; i < chaptesInOrder.length; i++) {
			if (chaptesInOrder[i].compareTo(chapter.getId().toString()) == 0) {
				String temp = chaptesInOrder[i];
				chaptesInOrder[i] = chaptesInOrder[i - 1];
				chaptesInOrder[i - 1] = temp;
			}
		}
		String newOrderRaw = arrayOrderToStringOrder(chaptesInOrder);
		comic.setChapters(newOrderRaw);
		comicRepository.save(comic);
		return newOrderRaw;
	}

	public String downChapterOrder(Chapter chapter) {
		Long comicId = chapter.getComicId();
		Comic comic = comicService.findById(comicId);
		String chapterOrderRaw = comic.getChapters();
		String[] chaptesInOrder = chapterOrderRaw.split(",");
		for (int i = 0; i < chaptesInOrder.length - 1; i++) {
			if (chaptesInOrder[i].compareTo(chapter.getId().toString()) == 0) {
				String temp = chaptesInOrder[i];
				chaptesInOrder[i] = chaptesInOrder[i + 1];
				chaptesInOrder[i + 1] = temp;
				break;
			}
		}
		String newOrderRaw = arrayOrderToStringOrder(chaptesInOrder);
		comic.setChapters(newOrderRaw);
		comicRepository.save(comic);
		return newOrderRaw;
	}

	public List<Chapter> sortByOrderChapter(Long comicId, List<Chapter> inputChapter) {
		Comic comic = comicService.findById(comicId);
		String orderChapterString = comic.getChapters();
		if(orderChapterString == null) orderChapterString = "";
		String[] order = orderChapterString.split(",");
		List<Long> orderIds = new ArrayList<>();
		for (int i = 0; i < order.length; i++) {
			if (order[i].isEmpty() == false && order[i].isBlank() == false)
				orderIds.add(new Long(order[i]));
		}
		List<Chapter> resultOrder = new ArrayList<>();
		for (int i = 0; i < orderIds.size(); i++) {
			for (int j = 0; j < inputChapter.size(); j++) {
				if (orderIds.get(i).equals(inputChapter.get(j).getId())) {
					resultOrder.add(inputChapter.get(j));
				}
			}
		}
		return resultOrder;

	}
}
