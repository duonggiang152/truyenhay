package com.TruyenHay.TruyenHayWebAPI.Services.Comic;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SearchComicObject {
	private String tag;
	private List<String> tags;
	private String descriptionSub;
	private String nameSub;
}
