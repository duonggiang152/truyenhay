package com.TruyenHay.TruyenHayWebAPI.Services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ImageService {
	public String storeImage(MultipartFile file) throws Exception {
		String uploadDir = System.getProperty("user.dir") + "/images/";
		String name = UUID.randomUUID() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());
		String path = uploadDir + name ;
		File newFile = new File(path);
		file.transferTo(newFile);
		return name;
	}
}
