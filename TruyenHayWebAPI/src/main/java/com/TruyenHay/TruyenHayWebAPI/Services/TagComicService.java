package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import com.TruyenHay.TruyenHayWebAPI.Repo.TagComicRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TagComicService {
	private final TagComicRepository tagComicRepository;

	public TagComic findTagByName(String name) {
		Optional<TagComic> tagComic = tagComicRepository.findByName(name);
		if(tagComic.isPresent() == false) {
			throw  new HTTPErrorException("Tag not exist", HttpStatus.BAD_REQUEST);
		};
		return tagComic.get();
	}
	public Boolean isTagExist(String name) {
		Optional<TagComic> tagComic = tagComicRepository.findByName(name);
		return tagComic.isPresent();
	}

	public List<TagComic> getAllTag() {
		return tagComicRepository.findAll();
	}

//	public boolean isContainAnyComic(TagComic tagComic) {
//		return isContainAnyComic(tagComic.getId());
//	}
//
//	public boolean isContainAnyComic(Long id ) {
//
//	}
}
