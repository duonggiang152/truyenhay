package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Repo.ChapterRepository;
import com.TruyenHay.TruyenHayWebAPI.Repo.PageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChapterService {
	private final ChapterRepository chapterRepository;
	private final PageRepository pageRepository;

	private final OrderChapterService orderChapterService;

	public void delete(Long id) {
		Chapter chapter = findById(id);
		orderChapterService.deleteComicTraceChapter(chapter.getComicId(), chapter.getId());
		chapterRepository.deleteById(id);

	}

	public Chapter findById(Long id) {
		Optional<Chapter> chapter = chapterRepository.findById(id);
		if (chapter.isPresent()) return chapter.get();
		return null;
	}

	public Chapter update(Long id, String name) {
		Chapter chapter = findById(id);
		chapter.setChapterName(name);
		chapterRepository.save(chapter);
		return chapter;
	}

	public Page addPage(String name, Long id) {
		Page page = new Page();
		page.setTittle(name);
		page.setChapterId(id);
		page.setChapterId(id);

		pageRepository.save(page);
		Long pageId = page.getId();
		Chapter chapter = findById(id);
		if(chapter.getPages().isEmpty() == false && chapter.getPages().isBlank() == false ) {
			chapter.setPages(chapter.getPages() + "," + String.valueOf(pageId));
		}
		else {
			chapter.setPages(String.valueOf(pageId));
		}
		chapterRepository.save(chapter);
		return page;
	}

	public List<Chapter> getAllChapter(Long comicId) {

		List<Chapter> rawChapter =  chapterRepository.findByComicId(comicId);
		return orderChapterService.sortByOrderChapter(comicId, rawChapter);
	}
}
