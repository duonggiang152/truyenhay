package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Exception.Constants;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Repo.UserRepository;
import com.TruyenHay.TruyenHayWebAPI.enums.UserRole;
import com.TruyenHay.TruyenHayWebAPI.enums.UserStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
	private final UserRepository userRepo;

	public User register(String username, String password) {
		try {
			User newUser = new User();
			newUser.setUserName(username);
			newUser.setHashedPassword(password);
			newUser.setRole(UserRole.USER);
			newUser.setStatus(UserStatus.ENABLE);
			userRepo.save(newUser);

			return newUser;
		} catch (Exception exception) {
			throw exception;
		}
	}

	public String genSalt() {
		return BCrypt.gensalt();
	}

	public String hashPassword(String password) {
		return BCrypt.hashpw(password, genSalt());
	}

	public boolean checkPassword(String password, String hashedPassword) {
		return BCrypt.checkpw(password, hashedPassword);
	}
	public User updateAvatar(String username, String avatarpath) {
		Optional<User> userQueryObject = userRepo.findByUserName(username);
		if (userQueryObject.isPresent() == false) return null;
		User user = userQueryObject.get();
		user.setAvatar(avatarpath);
		userRepo.save(user);
		return user;
	}

	public User findByName(String username) {
		try {
			Optional<User> userQueryObject = userRepo.findByUserName(username);
			if (userQueryObject.isPresent() == false) return null;
			return userQueryObject.get();
		} catch (Exception exception) {
			System.out.println(exception);
			throw exception;
		}

	}

	public List<User> findAll() {
		return userRepo.findAll();
	}

	public User changeStatus(Long userId ,UserStatus userStatus) {
		User user = findById(userId);
		user.setStatus(userStatus);
		userRepo.save(user);
		return user;
	}

	public List<User> getUserForAdminPage() {
		List<User> result = new ArrayList<>();
		List<User> allCurrentUser = findAll();
		allCurrentUser.forEach(user ->
		{
			if(user.getRole() != UserRole.ADMIN) {
				result.add(user);
			}
		});
		return result;
	}

	public User findById(Long id) {
		try {
			Optional<User> userQueryObject = userRepo.findById(id);
			if (userQueryObject.isPresent() == false) return null;
			return userQueryObject.get();
		} catch (Exception exception) {
			System.out.println(exception);
			throw exception;
		}
	}
}
