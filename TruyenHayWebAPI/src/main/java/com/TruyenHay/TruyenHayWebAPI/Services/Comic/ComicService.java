package com.TruyenHay.TruyenHayWebAPI.Services.Comic;

import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import com.TruyenHay.TruyenHayWebAPI.Repo.ChapterRepository;
import com.TruyenHay.TruyenHayWebAPI.Repo.ComicRepository;
import com.TruyenHay.TruyenHayWebAPI.Repo.TagComicRepository;
import com.TruyenHay.TruyenHayWebAPI.Services.TagComicService;
import jakarta.persistence.criteria.*;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class ComicService {
	private final ComicRepository comicRepo;
	private final TagComicRepository tagComicRepository;
	private final TagComicService tagComicService;

	private final ChapterRepository chapterRepository;
	public Comic findByName(String name) {
		Optional<Comic> optionalComic = comicRepo.findByName(name);
		return  optionalComic.get();
	}

	public Comic findById(Long id) {
		Optional<Comic> optionalComic = comicRepo.findById(id);
		return  optionalComic.get();
	}

	public Boolean isIdExist(Long id) {
		Optional<Comic> optionalComic = comicRepo.findById(id);
		return optionalComic.isPresent();
	}

	public void verifyTagComic(List<String> tagsName) {
		tagsName.forEach( tagname -> {
			if(tagComicService.isTagExist(tagname) == false) {
				TagComic newTag = new TagComic();
				newTag.setName(tagname);
				tagComicRepository.save(newTag);
			}
		});
	}

	public Chapter addChapter(String name, Long comicId) {
		if(isIdExist(comicId) == false) throw new HTTPErrorException("id not exist", HttpStatus.BAD_REQUEST);
		Comic comic = findById(comicId);
		Chapter chapter = new Chapter();
		chapter.setChapterName(name);
		chapter.setComicId(comicId);
		chapter.setChapterNumber(comic.getLatestChapter());
		comic.setLatestChapter(comic.getLatestChapter().longValue()+1);
		chapterRepository.save(chapter);
		Long chapterId = chapter.getId();
		if(comic.getChapters().isEmpty() == false && comic.getChapters().isBlank() == false ) {
			comic.setChapters(comic.getChapters() + "," + String.valueOf(chapterId));
		}
		else {
			comic.setChapters(String.valueOf(chapterId));
		}
		comicRepo.save(comic);

		return chapter;
	}

	@Transactional
	public Comic createComic(String name, List<String> tagsName, String description) {
		verifyTagComic(tagsName);
		Comic comic = new Comic();
		comic.setName(name);
		comic.setTags(new HashSet<>());
		comic.setDescription(description);
		comic.setChapters("");
		comicRepo.save(comic);

		tagsName.forEach(tagName -> {
			TagComic tag = tagComicService.findTagByName(tagName);
			comic.getTags().add(tag);

			comicRepo.save(comic);
			tagComicRepository.save(tag);
		});
		return comic;
	}
	public List<Comic> findAll() {
		return comicRepo.findAll();
	}

	public Comic UpdateComic(Long id, String name, List<String> tagsName, String description){
		verifyTagComic(tagsName);
		Comic comic = findById(id);
		comic.setName(name);
		Set setTagsName = new HashSet();
		tagsName.forEach(tagName -> {
			setTagsName.add(tagComicService.findTagByName(tagName));
		});
		comic.setTags(setTagsName);
		comic.setDescription(description);
		comicRepo.save(comic);
		return comic;

	}

	public Comic setAvatar(Long id, String avatar) {
		if(isIdExist(id) == false) throw new HTTPErrorException("id not exist", HttpStatus.BAD_REQUEST);
		Comic comic = findById(id);
		comic.setAvatarUrl(avatar);
		comicRepo.save(comic);
		return comic;
	}

	public Comic deleteComic(Long id) {
		Comic comic = findById(id);
		comicRepo.delete(comic);

		return comic;
	}

	private Specification<Comic> tagSpect(String tagName) {
		return (root, query, builder) -> {
			if (tagName == null) {
				return null;
			} else {
				Join<Comic, TagComic> tagJoin = root.join("tags", JoinType.INNER);
				return builder.equal(tagJoin.get("name"), tagName);
			}
		};
	}

	private Specification<Comic> descriptionSpect(String description) {
		return (root, query, builder) -> {
			if (description == null) {
				return null;
			} else {
				return builder.like(root.get("description"), "%" + description + "%");
			}
		};
	}

	private Specification<Comic> nameSpect(String name) {
		return (root, query, builder) -> {
			if (name == null) {
				return null;
			} else {
				return builder.like(root.get("name"), "%" + name + "%");
			}
		};
	}

	public List<Comic> search(SearchComicObject searchObject) {
		Specification specification = Specification.where(null);
		if(searchObject.getTag() != null) {
			specification = specification.and(tagSpect(searchObject.getTag()));
		}
		Specification orspecification = Specification.where(null);
		if(searchObject.getDescriptionSub() != null) {
			orspecification = orspecification.or(descriptionSpect(searchObject.getDescriptionSub()));
		}
		if(searchObject.getNameSub() != null) {
			orspecification = orspecification.or(nameSpect(searchObject.getNameSub()));
		}
		return comicRepo.findAll(specification.and(orspecification));

	}
}
