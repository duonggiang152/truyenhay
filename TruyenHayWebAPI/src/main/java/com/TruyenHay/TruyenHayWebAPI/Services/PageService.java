package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Repo.ChapterRepository;
import com.TruyenHay.TruyenHayWebAPI.Repo.PageRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor

public class PageService {
	private final PageRepository pageRepository;
	private final ChapterRepository chapterRepository;
	private final OrderPageService orderPageService;

	public List<Page> findPageByChapter(Long id)  {

		List<Page> pages =  pageRepository.findByChapterId(id);
		return orderPageService.orderChapter(id, pages);
	}


	public Page findById(Long id) {
		Optional<Page> page = pageRepository.findById(id);
		if (page.isPresent()) return page.get();
		throw new HTTPErrorException("Page not found", HttpStatus.NOT_FOUND);
	}

	public Page delete(Long id) {
		Optional<Page> page = pageRepository.findById(id);
		if(page.isPresent() == false) return null;
		orderPageService.deleteChapterTracePages(page.get().getChapterId(), id);
		pageRepository.deleteById(id);
		return page.get();

	}

	public Page addImage(String path, Long id) {
		Optional<Page> optionalPage = pageRepository.findById(id);
		if(optionalPage.isPresent() == false) throw new HTTPErrorException("page not exist", HttpStatus.BAD_REQUEST);
		Page page = optionalPage.get();
		page.setPath(path);
		pageRepository.save(page);
		return page;
	}
}
