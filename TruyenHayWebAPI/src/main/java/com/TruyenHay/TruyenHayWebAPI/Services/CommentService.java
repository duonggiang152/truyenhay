package com.TruyenHay.TruyenHayWebAPI.Services;

import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.Comment;
import com.TruyenHay.TruyenHayWebAPI.Repo.ComicRepository;
import com.TruyenHay.TruyenHayWebAPI.Repo.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CommentService {
	private final ComicRepository comicRepository;
	private final CommentRepository commentRepository;
	private final UserService userService;
	private final ChapterService chapterService;

	public Comment addComent(Long userId, Long chapterId, String comment) {
		if(userService.findById(userId) == null) {
			throw new HTTPErrorException("userID not exist", HttpStatus.BAD_REQUEST);
		}
		if(chapterService.findById(chapterId) == null) {
			throw new HTTPErrorException("chapter not exist", HttpStatus.BAD_REQUEST);
		}
		comment = comment.trim();
		if(comment.isBlank() == false && comment.isEmpty() == false) {
			Comment commentEntity = new Comment();
			commentEntity.setChapterId(chapterId);
			commentEntity.setUserId(userId);
			commentEntity.setComment(comment);
			commentRepository.save(commentEntity);
			return commentEntity;
		}
		else {
			throw new HTTPErrorException("comment emty", HttpStatus.BAD_REQUEST);
		}
	}

	public List<Comment> findAllComment(Long chapterId) {
		return commentRepository.findByChapterId(chapterId);
	}
}
