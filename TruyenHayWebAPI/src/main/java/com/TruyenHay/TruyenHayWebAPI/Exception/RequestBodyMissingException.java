package com.TruyenHay.TruyenHayWebAPI.Exception;

import org.springframework.http.converter.HttpMessageNotReadableException;

public class RequestBodyMissingException extends HttpMessageNotReadableException {
	public RequestBodyMissingException() {
		super("Required request body is missing");
	}
}