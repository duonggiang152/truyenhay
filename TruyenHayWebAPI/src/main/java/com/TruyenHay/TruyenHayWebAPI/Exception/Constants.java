package com.TruyenHay.TruyenHayWebAPI.Exception;

import lombok.Data;

@Data
public class Constants {
	public static final String HTTP_ERR_USERNOTEXIST = "USER_NOT_EXIST";
}
