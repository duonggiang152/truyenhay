package com.TruyenHay.TruyenHayWebAPI;

import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Repo.UserRepository;
import com.TruyenHay.TruyenHayWebAPI.Services.UserService;
import com.TruyenHay.TruyenHayWebAPI.enums.UserRole;
import com.TruyenHay.TruyenHayWebAPI.enums.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@SpringBootApplication
public class TruyenHayWebApiApplication implements CommandLineRunner {
	@Autowired
	private  UserService userService;

	@Autowired
	private  UserRepository userRepository;

	public static void main(String[] args) {
		String currentPath = System.getProperty("user.dir");

		// Define the folder name where you want to store images
		String folderName = "images";

		// Create a Path object for the folder
		Path folderPath = Paths.get(currentPath, folderName);

		// Check if the folder exists
		if (!Files.exists(folderPath)) {
			try {
				// Create the folder if it doesn't exist
				Files.createDirectories(folderPath);
				System.out.println("Image folder created successfully.");
			} catch (IOException e) {
				System.err.println("Failed to create the image folder: " + e.getMessage());
			}
		} else {
			System.out.println("Image folder already exists.");
		}

		//update status, ROLE


		SpringApplication.run(TruyenHayWebApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		List<User> users = userRepository.findAll();
//		users.forEach(user -> {
//			if(user.getRole() == UserRole.USER || user.getRole() == null) {
//				user.setRole(UserRole.USER);
//
//				userRepository.save(user);
//			}
//			user.setStatus(UserStatus.ENABLE);
//			userRepository.save(user);
//		});
	}
}
