package com.TruyenHay.TruyenHayWebAPI.Repo;

import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Long> {
	List<Chapter> findByComicId(Long id);
}
