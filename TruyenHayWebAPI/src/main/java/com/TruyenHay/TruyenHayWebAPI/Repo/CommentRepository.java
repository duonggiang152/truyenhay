package com.TruyenHay.TruyenHayWebAPI.Repo;

import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Model.Comment;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
	List<Comment> findByChapterId(Long id);
}
