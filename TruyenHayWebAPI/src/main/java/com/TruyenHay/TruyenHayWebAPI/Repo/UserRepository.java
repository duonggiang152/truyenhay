package com.TruyenHay.TruyenHayWebAPI.Repo;

import com.TruyenHay.TruyenHayWebAPI.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findById(Long id);
	Optional<User> findByUserName(String username);
}
