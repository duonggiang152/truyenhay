package com.TruyenHay.TruyenHayWebAPI.Repo;

import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TagComicRepository extends JpaRepository<TagComic, Long> {
	@Override
	Optional<TagComic> findById(Long aLong);

	Optional<TagComic> findByName(String name);
}
