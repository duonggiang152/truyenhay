package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.Page.CreatePageDTO;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Services.ChapterService;
import com.TruyenHay.TruyenHayWebAPI.Services.ImageService;
import com.TruyenHay.TruyenHayWebAPI.Services.PageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/page")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class pageController {
	private final PageService pageService;
	private final ImageService imageService;

	private final ChapterService chapterService;
	@PostMapping
	public ResponseEntity<Page> createPage(@RequestBody CreatePageDTO createPageDTO) {
		Page page = chapterService.addPage(createPageDTO.getTittle(), createPageDTO.getChapterId());
		return ResponseEntity.ok(page);
	}

	@GetMapping
	public ResponseEntity<List<Page>> getAllPage(@RequestParam("chapter") Long id) {

		return ResponseEntity.ok(pageService.findPageByChapter(id));
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Page> deletePage(@PathVariable("id") Long id) {
		return ResponseEntity.ok(pageService.delete(id));
	}
	@PostMapping("/update-image")
	public ResponseEntity<Page> updatePage(
			@RequestParam("page") MultipartFile pageImage,
			@RequestParam("id") Long pageId
			) {
		try {
			String path = imageService.storeImage(pageImage);
			Page page = pageService.addImage(path, pageId);
			return  ResponseEntity.ok(page);
		}
		catch (Exception exception) {
			throw new HTTPErrorException("unknow exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
