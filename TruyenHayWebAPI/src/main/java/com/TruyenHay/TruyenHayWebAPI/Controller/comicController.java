package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.Comic.*;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import com.TruyenHay.TruyenHayWebAPI.Services.*;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.ComicService;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.SearchComicObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/comic")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class comicController {
	private final ComicService comicService;
	private final ImageService imageService;

	private final OrderChapterService orderChapterService;

	private final ChapterService chapterService;
	private final TagComicService tagComicService;

	@GetMapping("/{id}")
	public ResponseEntity<Comic> getComic(@PathVariable("id") Long id) {

		if (id == null) {
			return ResponseEntity.ok(null);
		} else {
			return ResponseEntity.ok(comicService.findById(id));
		}

	}

	@PostMapping("/update/{id}")
	public ResponseEntity<Comic> updateComic(@PathVariable("id") Long id,
											 @RequestParam("name") String name,
											 @RequestParam("tags") List<String> tags,
											 @RequestParam("description") String description
	) {

		return ResponseEntity.ok(comicService.UpdateComic(id, name, tags, description));
	}
	@PostMapping("/banner")
	public ResponseEntity<UpdateBannerResponse> updateBanerComic(
			@RequestParam("banner") MultipartFile file, @RequestParam("comicId") long comicId
	) {
		try {

			String path = imageService.storeImage(file);
			Comic comic = comicService.setAvatar(comicId, path);
			return ResponseEntity.ok(new UpdateBannerResponse(comic));
		} catch (Exception exception) {
			throw new HTTPErrorException(exception.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping()
	public ResponseEntity<List<Comic>> getComic(
			@RequestParam(value="tag", required = false) String tag,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description) {
		SearchComicObject searchComicObject = new SearchComicObject();
		searchComicObject.setNameSub(name);
		searchComicObject.setTag(tag);
		searchComicObject.setDescriptionSub(description);

			return ResponseEntity.ok(comicService.search(searchComicObject));

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Comic> deleteComic(@PathVariable("id") Long id) {
		return ResponseEntity.ok(comicService.deleteComic(id));
	}

	@PostMapping
	public ResponseEntity<CreateComicResponse> createComic(@RequestBody CreateComicRequest createComicRequest) {
		try {
			Comic comic = comicService.createComic(createComicRequest.getName(), createComicRequest.getTagsName(), createComicRequest.getDescription());
			return ResponseEntity.ok(new CreateComicResponse(comic));
		} catch (Exception exception) {
			System.out.println(exception.toString());
			throw exception;
		}
	}



	@PostMapping("/up-chapter")
	public ResponseEntity<String> upChapterOrder( @RequestBody ChangeChapterDTO changeChapterDTO) {
		return ResponseEntity.ok(orderChapterService.upChapterOrder(chapterService.findById(changeChapterDTO.getChapterId())));
	}
	@PostMapping("/down-chapter")
	public ResponseEntity<String> downChapterOrder(@RequestBody ChangeChapterDTO changeChapterDTO) {
		return ResponseEntity.ok(orderChapterService.downChapterOrder(chapterService.findById(changeChapterDTO.getChapterId())));
	}
}
