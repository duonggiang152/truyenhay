package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.User.LoginRequest;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.LoginResponse;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.RegisterRequest;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Model.UserDetailsImplementation;
import com.TruyenHay.TruyenHayWebAPI.Services.JwtService;
import com.TruyenHay.TruyenHayWebAPI.Services.UserDetailsServiceImplement;
import com.TruyenHay.TruyenHayWebAPI.Services.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/authentication")
@RequiredArgsConstructor
@Data
//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class authenticationController {
	private final UserService userService;
	private final JwtService jwtService;

	private final UserDetailsServiceImplement userDetailsServiceImplement;

	@PostMapping("/login")
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
		if (userService.findByName(loginRequest.getUserName()) == null)
			throw new HTTPErrorException("username not exist", HttpStatus.BAD_REQUEST);
		User user = userService.findByName(loginRequest.getUserName());
		if (!userService.checkPassword(loginRequest.getPassword(),user.getHashedPassword())) {
			throw new HTTPErrorException("password not match", HttpStatus.BAD_REQUEST);
		}
		UserDetails userDetails =
				userDetailsServiceImplement.loadUserByUsername(
						loginRequest.getUserName()
				);
		return ResponseEntity.ok(new LoginResponse(jwtService.generateToken(userDetails)));
	}


	@PostMapping("/register")
	public ResponseEntity<String> register(@RequestBody RegisterRequest registerRequest) {
		if (userService.findByName(registerRequest.getUserName()) != null)
			throw new HTTPErrorException("username exist", HttpStatus.BAD_REQUEST);
		userService.register(registerRequest.getUserName(), userService.hashPassword(registerRequest.getPassword()));
		UserDetailsImplementation userDetails = new UserDetailsImplementation();
		userDetails.setUserName(registerRequest.getUserName());
		return ResponseEntity.ok(jwtService.generateToken(userDetails));
	}
}
