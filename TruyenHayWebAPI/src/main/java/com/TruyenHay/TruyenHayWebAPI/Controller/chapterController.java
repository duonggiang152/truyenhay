package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.Chapter.CreateChapterDTO;
import com.TruyenHay.TruyenHayWebAPI.DTO.Chapter.ChangePageOrderDTO;
import com.TruyenHay.TruyenHayWebAPI.DTO.Chapter.UpdateChapterDTO;
import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Services.*;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.ComicService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chapter")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class chapterController {
	final ChapterService chapterService;
	final ComicService comicService;

	final OrderChapterService orderChapterService;

	final OrderPageService orderPageService;
	final PageService pageService;
	@PostMapping()
	public ResponseEntity<Chapter> addChapter(@RequestBody CreateChapterDTO createChapterDTO) {
		Chapter chapter = comicService.addChapter(createChapterDTO.getName(), createChapterDTO.getComicId());
		return ResponseEntity.ok(chapter);

	}
	@PutMapping()
	public ResponseEntity<Chapter> updateChapter(@RequestBody UpdateChapterDTO updateChapterDTO) {
		return ResponseEntity.ok(chapterService.update(updateChapterDTO.getId(), updateChapterDTO.getName()));

	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteChapter(@PathVariable("id") Long chapterId) {
		chapterService.delete(chapterId);
		return ResponseEntity.ok(true);
	}
	@GetMapping("/{id}")
	public ResponseEntity<List<Chapter>> findChapter(@PathVariable("id") Long id) {
		return ResponseEntity.ok(chapterService.getAllChapter(id));
	}

	@PostMapping("/up-page")
	public  ResponseEntity<String> upPageChapter(@RequestBody ChangePageOrderDTO changePageOrderDTO) {
		return ResponseEntity.ok(orderPageService.upPageOrder(pageService.findById(changePageOrderDTO.getPageId())));
	}

	@PostMapping("/down-page")
	public  ResponseEntity<String> downPageChapter(@RequestBody ChangePageOrderDTO changePageOrderDTO) {
		return ResponseEntity.ok(orderPageService.downPageOrder(pageService.findById(changePageOrderDTO.getPageId())));
	}

}
