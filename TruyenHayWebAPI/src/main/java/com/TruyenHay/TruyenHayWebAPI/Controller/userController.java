package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.User.UpdateAvatarResponse;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.PublicUserInfo;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.UpdateReponseStatus;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.UserInfoResponse;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Services.ImageService;
import com.TruyenHay.TruyenHayWebAPI.Services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class userController {
	private final UserService userService;
	private final ImageService imageService;
	@GetMapping
	public ResponseEntity<List<PublicUserInfo>> getAllUser() {
		List<User> userList = userService.getUserForAdminPage();
		List<PublicUserInfo> publicUserInfos = new ArrayList<>();
		userList.forEach(user -> {
			PublicUserInfo publicUserInfo = new PublicUserInfo();
			publicUserInfo.setUserName(user.getUserName());
			publicUserInfo.setStatus(user.getStatus());
			publicUserInfo.setId(user.getId());
			publicUserInfos.add(publicUserInfo);
		});
		return ResponseEntity.ok(publicUserInfos);
	}

	@PostMapping("/status")
	public ResponseEntity<PublicUserInfo> updateStatus(@RequestBody UpdateReponseStatus updateReponseStatus) {
		User user =  userService.changeStatus(updateReponseStatus.getId(), updateReponseStatus.getUserStatus());
		PublicUserInfo userInfoResponse = new PublicUserInfo();
		userInfoResponse.setUserName(userInfoResponse.getUserName());
		userInfoResponse.setId(user.getId());
		userInfoResponse.setStatus(user.getStatus());
		return ResponseEntity.ok(userInfoResponse);
	}
	@GetMapping("/info")
	public ResponseEntity<UserInfoResponse> getUserInfo(Authentication authentication) {
		UserInfoResponse response = new UserInfoResponse("giang", "giagn@gmail.com");
		return ResponseEntity.ok(response);
	}



	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/avatar")
	public ResponseEntity<UpdateAvatarResponse> uploadImage(@RequestParam("avatar") MultipartFile file,@AuthenticationPrincipal UserDetails userDetails) {
		try {
			// Check if the file is not empty
			System.out.println(userDetails.getUsername());
			if (!file.isEmpty()) {
				try {
					// Save the file to a specific location (you can also save to a database)
					String filePPath = imageService.storeImage(file);
					userService.updateAvatar(userDetails.getUsername(), filePPath);
					return ResponseEntity.ok(new UpdateAvatarResponse("Success", null));
				} catch (IOException e) {
					System.out.println(e);
					return ResponseEntity.ok(new UpdateAvatarResponse("Failed", "Up load data failed"));
				}
			} else {
				return ResponseEntity.ok(new UpdateAvatarResponse("Failed", "Up load data failed"));
			}
		} catch (Exception exception) {
			System.out.println(exception);
			throw new HTTPErrorException("unknow exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



}
