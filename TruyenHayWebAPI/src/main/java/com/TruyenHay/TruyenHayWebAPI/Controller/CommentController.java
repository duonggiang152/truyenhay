package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.DTO.Comment.CommentDetailResponseDto;
import com.TruyenHay.TruyenHayWebAPI.DTO.Comment.CreateCommentRequestDto;
import com.TruyenHay.TruyenHayWebAPI.DTO.User.PublicUserInfo;
import com.TruyenHay.TruyenHayWebAPI.Model.Comment;
import com.TruyenHay.TruyenHayWebAPI.Model.User;
import com.TruyenHay.TruyenHayWebAPI.Services.CommentService;
import com.TruyenHay.TruyenHayWebAPI.Services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/comment")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CommentController {
	private final UserService userService;
	private final CommentService commentService;
	@PostMapping
	public Comment addComment(@RequestBody CreateCommentRequestDto createCommentRequestDto, @AuthenticationPrincipal UserDetails userDetails) {
		String username = userDetails.getUsername();
		User user = userService.findByName(username);
		return commentService.addComent(user.getId(), createCommentRequestDto.getChapterId(), createCommentRequestDto.getComment());
	}

	@GetMapping
	public ResponseEntity<List<CommentDetailResponseDto>> getComment(@RequestParam("chapter") Long chapterId) {
		List<CommentDetailResponseDto> commentDetailResponseDtos = new ArrayList<>();
		List<Comment> comments = commentService.findAllComment(chapterId);
		comments.forEach(comment -> {
			CommentDetailResponseDto commentDetailResponseDto = new CommentDetailResponseDto();
			commentDetailResponseDto.setComment(comment);
			User user = userService.findById(comment.getUserId());
			PublicUserInfo publicUserInfo = new PublicUserInfo();
			publicUserInfo.setUserName(user.getUserName());
			publicUserInfo.setId(user.getId());
			publicUserInfo.setStatus(user.getStatus());
			commentDetailResponseDto.setPublicUserInfo(publicUserInfo);
			commentDetailResponseDtos.add(commentDetailResponseDto);
		});
		return  ResponseEntity.ok(commentDetailResponseDtos);
	}
}
