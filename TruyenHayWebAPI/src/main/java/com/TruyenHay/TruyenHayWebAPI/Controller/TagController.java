package com.TruyenHay.TruyenHayWebAPI.Controller;

import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import com.TruyenHay.TruyenHayWebAPI.Services.TagComicService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TagController {
	final private TagComicService tagComicService;
	@GetMapping
	public ResponseEntity<List<TagComic>> getAllTag() {
		return ResponseEntity.ok(tagComicService.getAllTag());
	}
}
