package com.TruyenHay.TruyenHayWebAPI.Helper;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrantedAuthorityAccount implements GrantedAuthority {
	private String authority;
	@Override
	public String getAuthority() {
		return authority;
	}
}
