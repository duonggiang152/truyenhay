package com.TruyenHay.TruyenHayWebAPI.Advice;

import com.TruyenHay.TruyenHayWebAPI.DTO.ErrorDto;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Exception.RequestBodyMissingException;
import com.sun.tools.jconsole.JConsoleContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;

@ControllerAdvice
@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(HTTPErrorException.class)
	public ResponseEntity<ErrorDto> handleCustomException(HTTPErrorException ex) {
		ErrorDto errorResponse = new ErrorDto(ex.getMessage(), ex.getHttpStatus());
		return ResponseEntity.status(ex.getHttpStatus()).body(errorResponse);
	}
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<String> handleRequestBodyMissingException(HttpMessageNotReadableException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
	}
	@ExceptionHandler(MultipartException.class)
	public ResponseEntity<String> handleMultipartException(MultipartException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
	}



}
