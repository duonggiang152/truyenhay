package com.TruyenHay.TruyenHayWebAPI.DTO.Comic;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UpdateComicRequest {
	private String name;
	private String description;

	private List<String> tagsName;

}
