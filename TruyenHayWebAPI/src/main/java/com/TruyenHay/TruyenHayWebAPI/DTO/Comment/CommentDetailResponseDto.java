package com.TruyenHay.TruyenHayWebAPI.DTO.Comment;

import com.TruyenHay.TruyenHayWebAPI.DTO.User.PublicUserInfo;
import com.TruyenHay.TruyenHayWebAPI.Model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDetailResponseDto {
	private Comment comment;
	private PublicUserInfo publicUserInfo;
}
