package com.TruyenHay.TruyenHayWebAPI.DTO.Chapter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateChapterDTO {
	private Long id;
	private String name;
}
