package com.TruyenHay.TruyenHayWebAPI.DTO.Comment;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateCommentRequestDto {
	private Long chapterId;
	private String comment;
}
