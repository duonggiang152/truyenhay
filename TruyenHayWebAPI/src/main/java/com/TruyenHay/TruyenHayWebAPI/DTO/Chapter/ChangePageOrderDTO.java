package com.TruyenHay.TruyenHayWebAPI.DTO.Chapter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangePageOrderDTO {
	private Long pageId;
}
