package com.TruyenHay.TruyenHayWebAPI.DTO.User;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateAvatarResponse {
	private String status;
	private String message;
}
