package com.TruyenHay.TruyenHayWebAPI.DTO.Comic;

import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateComicResponse {
	private Comic newComic;
}
