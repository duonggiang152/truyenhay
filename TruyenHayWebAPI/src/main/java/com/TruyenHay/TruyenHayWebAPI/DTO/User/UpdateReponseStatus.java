package com.TruyenHay.TruyenHayWebAPI.DTO.User;

import com.TruyenHay.TruyenHayWebAPI.enums.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateReponseStatus {
	private Long id;
	private UserStatus userStatus;
}
