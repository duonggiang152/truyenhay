package com.TruyenHay.TruyenHayWebAPI.DTO.Page;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreatePageDTO {
	private String tittle;

	private Long chapterId;
}
