package com.TruyenHay.TruyenHayWebAPI.DTO.User;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class LoginRequest {
	private String userName;
	private String password;
}
