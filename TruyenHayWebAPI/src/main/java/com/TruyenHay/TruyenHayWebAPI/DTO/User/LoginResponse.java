package com.TruyenHay.TruyenHayWebAPI.DTO.User;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
	private String accessToken;
}
