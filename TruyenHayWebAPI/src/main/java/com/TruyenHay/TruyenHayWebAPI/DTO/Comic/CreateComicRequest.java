package com.TruyenHay.TruyenHayWebAPI.DTO.Comic;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CreateComicRequest {
	private String name;
	private List<String> tagsName;

	private String description;
}
