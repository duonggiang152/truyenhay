package com.TruyenHay.TruyenHayWebAPI.Configure;

import com.TruyenHay.TruyenHayWebAPI.Advice.GlobalExceptionHandler;
import com.TruyenHay.TruyenHayWebAPI.DTO.ErrorDto;
import com.TruyenHay.TruyenHayWebAPI.Exception.HTTPErrorException;
import com.TruyenHay.TruyenHayWebAPI.Services.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import netscape.javascript.JSObject;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.ErrorResponse;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class JWTFilter extends OncePerRequestFilter {
	private final JwtService jwtService;
	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private GlobalExceptionHandler globalExceptionHandler;
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		final String authHeader = request.getHeader("Authorization");
		try {
			if (authHeader == null || !authHeader.startsWith("Bearer ")) {
				filterChain.doFilter(request, response);
				return;
			}

			final String jwt = authHeader.substring(7);

			if(jwtService.isTokenExpired(jwt)) {
				throw new HTTPErrorException("JWT expired", HttpStatus.UNAUTHORIZED );
			}
			final String username = jwtService.extractUsername(jwt);
			if (username == null || SecurityContextHolder.getContext().getAuthentication() != null) {
				filterChain.doFilter(request, response);
				return;
			}
			try {
				UserDetails userDetails = userDetailsService.loadUserByUsername(username);
				if (jwtService.isTokenValid(jwt, userDetails)) {

					UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities() );
					authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}
				filterChain.doFilter(request, response);
				return;
			} catch (AuthenticationException e) {
				SecurityContextHolder.clearContext();
				response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());

			}
		} catch (Exception exception) {
			ResponseEntity responseEntity;
			if(exception instanceof ExpiredJwtException) {

				responseEntity = globalExceptionHandler.handleCustomException(new HTTPErrorException("JWT expired", HttpStatus.UNAUTHORIZED ));


			}

			else if(exception instanceof ExpiredJwtException) {
				responseEntity = globalExceptionHandler.handleCustomException(new HTTPErrorException("JWT expired", HttpStatus.UNAUTHORIZED ));

			}
			else responseEntity = globalExceptionHandler.handleCustomException(new HTTPErrorException("authorize err", HttpStatus.UNAUTHORIZED ));
			SecurityContextHolder.clearContext();
			if(responseEntity == null) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				response.getWriter().write(exception.getMessage());
			}
			else {
					response.setStatus(responseEntity.getStatusCode().value());
					response.getWriter().write(((ErrorDto)responseEntity.getBody()).getMessage().toString());


			}
		}
		filterChain.doFilter(request, response);
	}

}
