package com.TruyenHay.TruyenHayWebAPI.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Comic {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public Comic() {
		this.setLatestChapter(new Long("0"));
	}
	private String name;

	private String avatarUrl;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "comic_tag",  // Name of the association table
			joinColumns = @JoinColumn(name = "comic_id"),  // Foreign key column in the association table
			inverseJoinColumns = @JoinColumn(name = "tagComic_id")  // Foreign key column in the association table
	)
	private Set<TagComic> tags;

	private Long latestChapter;

	@Column(columnDefinition = "TEXT")
	private String description;


	private String chapters ;
}
