package com.TruyenHay.TruyenHayWebAPI;

import com.TruyenHay.TruyenHayWebAPI.Model.Chapter;
import com.TruyenHay.TruyenHayWebAPI.Model.Comic;
import com.TruyenHay.TruyenHayWebAPI.Model.Page;
import com.TruyenHay.TruyenHayWebAPI.Model.TagComic;
import com.TruyenHay.TruyenHayWebAPI.Repo.ComicRepository;
import com.TruyenHay.TruyenHayWebAPI.Services.*;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.ComicService;
import com.TruyenHay.TruyenHayWebAPI.Services.Comic.SearchComicObject;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

@SpringBootTest
public class ComicServiceTest {
	@Autowired
	private ComicService comicService;

	@Autowired
	private OrderPageService orderPageService;

	@Autowired
	private PageService pageService;

	@Autowired
	private OrderChapterService orderChapterService;

	@Autowired
	private ChapterService chapterService;

	@Autowired
	private ComicRepository comicRepository;
	@Test
	void test() {
//		ArrayList<String> tagsComic = new ArrayList<>();
//		tagsComic.add("hanh dong");
//		comicService.createComic("tes1", tagsComic);
	}
	@Test
	void test1() {
		comicService.setAvatar(new Long("11"), "hi");
	}

	@Test
	void addChapterTRest() {
		comicService.addChapter("dai chien phan 1", new Long("12"));
	}

	@Test
	void changeOrder() {
		Page page = pageService.findById(new Long("45"));
		orderPageService.upPageOrder(page);
	}

	@Test
	void createChapter() {
		comicService.addChapter("chapter test3",new Long("19"));
	}

	@Test
	void upChapter() {
		Chapter chapter = chapterService.findById(Long.valueOf(24));
		orderChapterService.upChapterOrder(chapter);
	}
	public Specification<Comic> tagSpec(String tagName) {
		return (root, query, builder) -> {
			if (tagName == null) {
				return null;
			} else {
				Join<Comic, TagComic> tagJoin = root.join("tags", JoinType.INNER);
				return builder.equal(tagJoin.get("name"), tagName);
			}
		};
	}
	@Test
	void ComicRepotest() {

		String tagName = "hanh dong";

//
//		Specifications<Comic> specifications = Specifications.where(descriptionSpec)
//				.and(nameSpec)
//				.and(tagSpec);
//		Specification<Comic> tagSpe1c = tagSpec(tagName);
//		System.out.println(comicService.search("Seventh Hokage"));
		SearchComicObject searchComicObject = new SearchComicObject();
		searchComicObject.setNameSub("One");
		List<Comic> comics = comicService.search(searchComicObject);
		System.out.println();
	}
//	@Test
//	void
}
