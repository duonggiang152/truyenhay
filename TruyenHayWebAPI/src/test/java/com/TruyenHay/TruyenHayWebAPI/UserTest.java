package com.TruyenHay.TruyenHayWebAPI;

import com.TruyenHay.TruyenHayWebAPI.Services.Comic.ComicService;
import com.TruyenHay.TruyenHayWebAPI.Services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {
	@Autowired
	private ComicService comicService;

	@Autowired
	private UserService userService;

	@Test
	void testUserService1() {
		String salt = userService.genSalt();
		String hashedPassword = userService.hashPassword("123123");
		Boolean isCompare = userService.checkPassword("123123", hashedPassword);

		System.out.println(salt);

		System.out.println(hashedPassword);
		System.out.println(isCompare);
	}
}
